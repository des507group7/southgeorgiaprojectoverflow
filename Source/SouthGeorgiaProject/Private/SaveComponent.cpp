// Fill out your copyright notice in the Description page of Project Settings.


#include "SaveComponent.h"
#include "QuestManager.h"
#include "PlayerUtilities.h"
#include "DialogueComponent.h"
#include <Engine.h>

// Sets default values for this component's properties
USaveComponent::USaveComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...

	
}


// Called when the game starts
void USaveComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	GetOwner()->InputComponent->BindAction("QuickSave", IE_Pressed, this, &USaveComponent::Save);
	GetOwner()->InputComponent->BindAction("QuickLoad", IE_Pressed, this, &USaveComponent::Load);
}


// Called every frame
void USaveComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void USaveComponent::Save()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("Game Save"));

	//Create an instance of the save game class
	UMySaveGame* saveGameInstance = Cast<UMySaveGame>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));
	
	//Update the values to be saved within this save instance
	saveGameInstance->_playerPosition = GetOwner()->GetActorLocation();
	saveGameInstance->_playerRotation = GetOwner()->GetActorRotation();

	//Save the objective
	if (UQuestManager* questManager = (UQuestManager*)GetOwner()->GetComponentByClass(UQuestManager::StaticClass()))
	{
		saveGameInstance->_currentObjectiveIndex = questManager->_currentObjective;
	}

	//Save the time period
	if (UPlayerUtilities* playerUtils = (UPlayerUtilities*)GetOwner()->GetComponentByClass(UPlayerUtilities::StaticClass()))
	{
		saveGameInstance->_currentTimePeriodIndex = playerUtils->_timePeriod;
	}

	TArray<AActor*> helpers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASaveGameHelper::StaticClass(), helpers);
	
	if (helpers.Num() > 0)
	{
		if(ASaveGameHelper* saveHelp = Cast<ASaveGameHelper>(helpers[0]))
			saveGameInstance->_doors = saveHelp->GetDoorData();
	}



	//Save the game
	UGameplayStatics::SaveGameToSlot(saveGameInstance, TEXT("Game Save 1"), 0);
}

void USaveComponent::Load()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("Game Load"));

	//Create an instance of the save game class
	UMySaveGame* saveGameInstance = Cast<UMySaveGame>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));
	//Load the saved game into the save game instance
	saveGameInstance = Cast<UMySaveGame>(UGameplayStatics::LoadGameFromSlot("Game Save 1", 0));
	//Update the player position and rotation loaded from the save game instance
	GetOwner()->SetActorLocation(saveGameInstance->_playerPosition);
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetControlRotation(saveGameInstance->_playerRotation);

	//Update the quest objective
	if (UQuestManager* questManager = (UQuestManager*)GetOwner()->GetComponentByClass(UQuestManager::StaticClass()))
	{
		questManager->OnLoad(saveGameInstance->_currentObjectiveIndex);
	}

	//Update the time period
	if (UPlayerUtilities* playerUtils = (UPlayerUtilities*)GetOwner()->GetComponentByClass(UPlayerUtilities::StaticClass()))
	{
		playerUtils->OnLoad(saveGameInstance->_currentTimePeriodIndex);
	}

	if (UDialogueComponent* dialogue = (UDialogueComponent*)GetOwner()->GetComponentByClass(UDialogueComponent::StaticClass()))
	{
		dialogue->FinishDialogue();
	}


	TArray<AActor*> helpers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASaveGameHelper::StaticClass(), helpers);

	if (helpers.Num() > 0)
	{
		if (ASaveGameHelper* saveHelp = Cast<ASaveGameHelper>(helpers[0]))
		{
			for (int i = 0; i < saveGameInstance->_doors.Num(); i++)
			{
				saveHelp->SetDoorData(saveGameInstance->_doors[i]);
			}
		}
	}
}