// Fill out your copyright notice in the Description page of Project Settings.


#include "TimeTravelTrigger.h"

// Sets default values for this component's properties
UTimeTravelTrigger::UTimeTravelTrigger()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTimeTravelTrigger::BeginPlay()
{
	Super::BeginPlay();

	// ...
	if (UPlayerUtilities* utils = (UPlayerUtilities*)GetWorld()->GetFirstPlayerController()->GetPawn()->GetComponentByClass(UPlayerUtilities::StaticClass()))
	{
		_playerUtils = utils;
	}
	
	_hasTriggered = false;
}


// Called every frame
void UTimeTravelTrigger::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTimeTravelTrigger::TriggerTimeTravel()
{
	if (_playerUtils != nullptr && !_hasTriggered)
	{
		_playerUtils->StartTimeTravelWithParam(_targetTimePeriod);
		_hasTriggered = true;
	}
}

