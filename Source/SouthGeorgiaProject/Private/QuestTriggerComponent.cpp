// Fill out your copyright notice in the Description page of Project Settings.


#include "QuestTriggerComponent.h"

// Sets default values for this component's properties
UQuestTriggerComponent::UQuestTriggerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UQuestTriggerComponent::BeginPlay()
{
	Super::BeginPlay();
	_hasBeenTriggered = false;
	// ...
	
}


// Called every frame
void UQuestTriggerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UQuestTriggerComponent::TriggerQuest()
{
	if (UQuestManager* questManager = UGameplayStatics::GetPlayerPawn(GetWorld(), 0)->FindComponentByClass<UQuestManager>())
	{
		if (questManager->_currentObjective == _linkedQuestIndex)
		{

			if (questManager->_onQuestTrigger.IsBound())
				questManager->_onQuestTrigger.Broadcast(_linkedQuestIndex);

			_hasBeenTriggered = true;
		}
	}
}