// Fill out your copyright notice in the Description page of Project Settings.


#include "Jigsaw.h"
#include "JigsawDrag.h"
#include "Components/CanvasPanel.h"
#include "JigsawManager.h"
#include "DialogueContainer.h"

 bool UJigsaw::NativeOnDrop(const FGeometry& MyGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* Operation)
{
	 Super::NativeOnDrop(MyGeometry, InDragDropEvent, Operation);

	 //Cast to our custom drag drop operation
	 UJigsawDrag* widgetDrag = Cast<UJigsawDrag>(Operation);

	 //Get the drag offset value from the custom operation
	 FVector2D dragOffset = widgetDrag->_dragOffset;
	 //Access the widget reference from the custom operation
	  UUserWidget* widgetRef = widgetDrag->_widgetReference;


	  widgetRef->RemoveFromParent();
	  widgetRef->AddToViewport();
	 FVector2D newPos =  GetCachedGeometry().AbsoluteToLocal(InDragDropEvent.GetScreenSpacePosition()) - dragOffset;
	
	 widgetRef->SetPositionInViewport(newPos, false);

	 widgetRef->SetDesiredSizeInViewport(widgetDrag->_widgetSize);
	 Cast<UJigsawPiece>(widgetRef)->UpdateCurrentPos(newPos);

	 CheckGameState();
	 return true;
}

 void UJigsaw::NativeOnInitialized()
 {
	 Super::NativeOnInitialized();

	 if(CompletedPuzzle != nullptr)
		 CompletedPuzzle->SetColorAndOpacity(FLinearColor(CompletedPuzzle->ColorAndOpacity.R, CompletedPuzzle->ColorAndOpacity.G, CompletedPuzzle->ColorAndOpacity.B, 0.0f));

	 _timerManager = &GetWorld()->GetTimerManager(); //Used to ensure the timer manager is valid

	 if (UCanvasPanel* RootWidget = Cast<UCanvasPanel>(GetRootWidget()))
	 {
		 TArray<UWidget*> puzzlePieces = RootWidget->GetAllChildren();

		 for (int i = 0; i < puzzlePieces.Num(); i++)
		 {
			 if (UJigsawPiece* current = Cast<UJigsawPiece>(puzzlePieces[i]))
			 {
				 current->_myParent = this;
				 _puzzlePieces.Add(current);
			 }
			 else if (UImage* placeholder = Cast<UImage>(puzzlePieces[i]))
			 {
				 if (puzzlePieces[i]->GetName() != "Border" || puzzlePieces[i]->GetName() != "Background")
				 {
					 _puzzleTrays.Add(placeholder);
				 }
			 }
		 }
	 }


}

 void UJigsaw::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
 {
	 Super::NativeTick(MyGeometry, InDeltaTime);

	 if (_isCompleted && CompletedPuzzle->ColorAndOpacity.A < 1.0f)
	 {
		 CompletedPuzzle->SetColorAndOpacity(FLinearColor(CompletedPuzzle->ColorAndOpacity.R, CompletedPuzzle->ColorAndOpacity.G, CompletedPuzzle->ColorAndOpacity.B, CompletedPuzzle->ColorAndOpacity.A + InDeltaTime));
	 }
 }

 void UJigsaw::CheckGameState()
 {
	 bool completed = true;
	 //Loop through all the puzzle pieces until one is found that is not in the correct place
	 //if the end of the loop is reached then all pieces are in place and the puzzle can begin the completion process
	 for (int i = 0; i < _puzzlePieces.Num(); i++)
	 {
		 if (!_puzzlePieces[i]->_isCorrectlyPlaced)
		 {
			 completed = false;
			 continue;
		 }
		 else
		 {
			 if (!_puzzlePieces[i]->_pushedToBack)
			 {
				 _puzzlePieces[i]->RemoveFromViewport();
				 _puzzlePieces[i]->AddToViewport(-10.0f);
				 _puzzlePieces[i]->_pushedToBack = true;
			 }
		 }
	 }

	 if (completed && !_isCompleted)
	 {
		 if(_puzzleComplete != nullptr)
			 UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), _puzzleComplete, GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorTransform(), true);
		


		 FTimerManager* tm = &GetWorld()->GetTimerManager(); //Used to ensure the timer manager is valid
		 //Set the completion tag to true
		 _isCompleted = true;

		 for (int i = 0; i < _puzzlePieces.Num(); i++)
		 {
			 _puzzlePieces[i]->_puzzleCompleted = true;
		 }

		UJigsawManager* manager =  (UJigsawManager*)GetWorld()->GetFirstPlayerController()->GetPawn()->GetComponentByClass(UJigsawManager::StaticClass());
		if (manager != nullptr)
		{
			//Call the finish puzzle function after the delay time has elapsed
			tm->SetTimer(_completedTimerHandle, manager, &UJigsawManager::FinishPuzzle, 3.0f, true);
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("Unable to find Jigsaw Manager on player"));
	 }
	 else
	 {
		 _isCompleted = false;
	 }
 }

 TArray<UJigsawPiece*> UJigsaw::GetAllPuzzlePieces()
 {
	 return _puzzlePieces;
 }

 void UJigsaw::Shuffle()
{

	 if (_puzzlePieces.Num() > 0)
	 {
		 //Store the last index for use in the shuffle
		 int lastIndex = _puzzlePieces.Num() - 1;

		 //Loop through the array and shuffle the pieces randomly
		 for (int i = 0; i <= lastIndex; i++)
		 {
			 int index = FMath::RandRange(i, lastIndex);

			 if (i != index)
				 _puzzlePieces.Swap(i, index);
		 }


		 //Loop through the now randomised list
		 //and place each piece at a corresponding location at the side of the screen
		 for (int i = 0; i <= lastIndex; i++)
		 {			 
			 if (_puzzleTrays.Num() > i)
			 {
				 UCanvasPanelSlot* _mySlot = Cast<UCanvasPanelSlot>(_puzzleTrays[i]->Slot);

				 if (_mySlot != nullptr)
				 {
					 FVector2D newLocation = _puzzleTrays[i]->GetCachedGeometry().AbsoluteToLocal(_mySlot->GetPosition());
					_puzzlePieces[i]->SetPosition(newLocation);
				 }
			 }
		 }
	 }
 }

 void UJigsaw::Cleanup()
 {
	 if (_timerManager != nullptr)
		 _timerManager->ClearTimer(_completedTimerHandle);
 }