// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactable.h"
#include "GameStateTrigger.h"

// Sets default values
AInteractable::AInteractable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AInteractable::BeginPlay()
{
	Super::BeginPlay();
	
	if (_dialogueContainer == nullptr)
		_dialogueContainer = (UDialogueContainer*)GetComponentByClass(UDialogueContainer::StaticClass());

	if (UQuestTriggerComponent* trigger = (UQuestTriggerComponent*)GetComponentByClass(UQuestTriggerComponent::StaticClass()))
	{
		_questTrigger = trigger;
	}

	_hasQuestBeenTriggered = false;
}

// Called every frame
void AInteractable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AInteractable::Interact()
{
	if (_dialogueContainer == nullptr)
		_dialogueContainer = (UDialogueContainer*)GetComponentByClass(UDialogueContainer::StaticClass());

	if (_dialogueContainer != nullptr)
	{
		_dialogueContainer->SetAsCurrentDialogue();
	}

	BlueprintInteract();


	
	if (_questTrigger != nullptr && !_hasQuestBeenTriggered)
	{
		_questTrigger->TriggerQuest();
		_hasQuestBeenTriggered = true;
	}


	if (UGameStateTrigger* gameStateTrigger = (UGameStateTrigger*)GetComponentByClass(UGameStateTrigger::StaticClass()))
	{
		gameStateTrigger->TriggerGameState();
	}

	/*else
	{
		UE_LOG(LogTemp, Warning, TEXT("Quest Trigger Not Found on %s"), *GetName());
		if (UQuestTriggerComponent* trigger = (UQuestTriggerComponent*)GetComponentByClass(UQuestTriggerComponent::StaticClass()))
		{
			trigger->TriggerQuest();
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Quest Trigger Still Not Found on %s"), *GetName());
		}
	}*/
}

