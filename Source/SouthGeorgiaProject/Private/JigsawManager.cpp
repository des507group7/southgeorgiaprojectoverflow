// Fill out your copyright notice in the Description page of Project Settings.


#include "JigsawManager.h"
#include "JigsawContainer.h"
#include "TimeTravelTrigger.h"

// Sets default values for this component's properties
UJigsawManager::UJigsawManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	//The player will default to not being in a puzzle
	_isInPuzzle = false;
}


// Called when the game starts
void UJigsawManager::BeginPlay()
{
	Super::BeginPlay();
	_playerUtils = (UPlayerUtilities*)GetOwner()->GetComponentByClass(UPlayerUtilities::StaticClass());
}


// Called every frame
void UJigsawManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UJigsawManager::UpdateCurrentPuzzle(TSubclassOf<UJigsaw> newPuzzle, UDialogueContainer* dialogue, UQuestTriggerComponent* questTrigger, UJigsawContainer* container)
{
	//Finish the existing puzzle if one is in progress
	if(_isInPuzzle)
		FinishPuzzle();

	//If the passed in puzzle is valid
	if (newPuzzle != nullptr)
	{
		//Update the stored puzzle class
		_currentPuzzleClass = newPuzzle;
	}

	_dialogue = dialogue;
	_questTrigger = questTrigger;	
	_jigsawContainer = container;
}

//This function begins the current puzzle
void UJigsawManager::BeginPuzzle()
{
	//Only proceed if not already in puzzle
	if (!_isInPuzzle)
	{
		//Check to ensure the puzzle class is valid
		if (_currentPuzzleClass != nullptr)
		{
			//Create a new jigsaw widget, deriving from the stored current puzzle class
			_currentPuzzle = (UJigsaw*)CreateWidget<UJigsaw>(GetWorld()->GetFirstPlayerController(), _currentPuzzleClass);

			//If the widget is successfully created
			if (_currentPuzzle != nullptr)
			{
				_currentPuzzle->Shuffle();
				//Add the widget to the viewport
				_currentPuzzle->AddToViewport();

				//Toggle the cursor on
				GetWorld()->GetFirstPlayerController()->bShowMouseCursor = true;
				//Set the flag to true
				_isInPuzzle = true;


				if (_playerUtils != nullptr)
					_playerUtils->_canControlPlayer = false;
				

			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Failed to create the puzzle widget"));
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No Puzzle Class To Create Widget From"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Puzzle Already In Progress"));
	}
}

//This function completes the current puzzle
void UJigsawManager::FinishPuzzle()
{
	//If a puzzle widget currently exists
	if (_currentPuzzle != nullptr)
	{
		_currentPuzzle->Cleanup();

		//Remove it from the viewport
		_currentPuzzle->RemoveFromViewport();
		//Return the cursor to hidden
		GetWorld()->GetFirstPlayerController()->bShowMouseCursor = false;
		//Set the flag to false
		_isInPuzzle = false;
		
		if (_dialogue != nullptr)
		{
			//Trigger the dialogue then lose the reference 
			_dialogue->SetAsCurrentDialogue();
			_dialogue = nullptr;
		}

		if (_questTrigger != nullptr)
		{
			//Trigger the quest
			_questTrigger->TriggerQuest();
			_questTrigger = nullptr;
		}

		if (_jigsawContainer != nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("Updating Container"));
			_jigsawContainer->_isCompleted = true;
			//Destroy Jigsaw Here???


			if (UTimeTravelTrigger* trigger = (UTimeTravelTrigger*)_jigsawContainer->GetOwner()->GetComponentByClass(UTimeTravelTrigger::StaticClass()))
			{
				trigger->TriggerTimeTravel();
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Not Updating Container"));
		}

		if (_playerUtils != nullptr)
			_playerUtils->_canControlPlayer = true;

	}
}

//This function returns the current puzzle status
//Used to disable player movement in the character blueprint when in a puzzle
bool UJigsawManager::GetPuzzleStatus()
{
	return _isInPuzzle;
}
