// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueContainer.h"

// Sets default values for this component's properties
UDialogueContainer::UDialogueContainer()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UDialogueContainer::BeginPlay()
{
	Super::BeginPlay();

	//Get the player Dialogue component

	APawn* playerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

	if(playerPawn != nullptr)
		_playerDialogue = playerPawn->FindComponentByClass<UDialogueComponent>();
	else
		UE_LOG(LogTemp, Warning, TEXT("Unable to find player pawn"));

}


// Called every frame
void UDialogueContainer::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UDialogueContainer::SetAsCurrentDialogue()
{


	if (_playerDialogue == nullptr)
	{
		APawn* playerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

		if (playerPawn != nullptr)
			_playerDialogue = playerPawn->FindComponentByClass<UDialogueComponent>();
	}

	//If the Player Dialogue component was successfully found
	if (_playerDialogue != nullptr)
	{
		if (_dialogue.Num() > 0)
		{
			/*if (UQuestTriggerComponent* component = (UQuestTriggerComponent*)GetOwner()->GetComponentByClass(UQuestTriggerComponent::StaticClass()))
			{
				component->TriggerQuest();
			}*/

			//Set the Dialogue Data in the player component to equal this container's dialogue
			_playerDialogue->SetCurrentDialogueData(_dialogue);
			//Tell the player dialogue component to begin playing
			_playerDialogue->BeginDialogue();
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Unable to find player dialogue component"));
	}
}

