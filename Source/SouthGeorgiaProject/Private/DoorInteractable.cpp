// Fill out your copyright notice in the Description page of Project Settings.


#include "DoorInteractable.h"

ADoorInteractable::ADoorInteractable()
{

}



void ADoorInteractable::SetOpen(bool open)
{
	_isOpen = open;

	if (_isOpen)
	{
		_prompt = _openPrompt;

		if (_closeSound != nullptr && _interactFirstTime)
			UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), _closeSound, GetActorTransform(), true);
	}
	else
	{
		_prompt = _closePrompt;

		if (_openSound != nullptr && _interactFirstTime)
			UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), _openSound, GetActorTransform(), true);
	}

	if (!_interactFirstTime)
		_interactFirstTime = true;
}

bool ADoorInteractable::GetOpen()
{
	return _isOpen;
}

void ADoorInteractable::BeginPlay()
{
	_interactFirstTime = false;
	_prompt = _openPrompt;
	Interact();

	if (_isLocked)
		_prompt = _lockedPrompt;
}

void ADoorInteractable::Interact()
{
	if (!_isLocked)
	{
		Super::Interact();
	}
	else
	{
		_prompt = _lockedPrompt;
	}
}

void ADoorInteractable::Unlock()
{
	_isLocked = false;
	_prompt = _openPrompt;
	Interact();
}