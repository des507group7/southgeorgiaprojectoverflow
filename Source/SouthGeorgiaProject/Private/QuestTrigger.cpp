// Fill out your copyright notice in the Description page of Project Settings.


#include "QuestTrigger.h"
#include "Kismet/GameplayStatics.h"
#include "Components/BillboardComponent.h"

// Sets default values for this component's properties
AQuestTrigger::AQuestTrigger()
{
	//Register Events
	OnActorBeginOverlap.AddDynamic(this, &AQuestTrigger::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &AQuestTrigger::OnOverlapEnd);

    _triggerComponent = (UQuestTriggerComponent*)FindComponentByClass<UQuestTriggerComponent>();

    if (_triggerComponent == nullptr)
        _triggerComponent = CreateDefaultSubobject<UQuestTriggerComponent>(TEXT("Quest Trigger Component"));
}




// Called when the game starts
void AQuestTrigger::BeginPlay()
{
    Super::BeginPlay();

    if(_drawDebugLines)
        DrawDebugBox(GetWorld(), GetActorLocation(), GetComponentsBoundingBox().GetExtent(), FColor::Purple, true, -1, 0, 5);


}

void AQuestTrigger::OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor)
{
    if (OtherActor != GetWorld()->GetFirstPlayerController()->GetPawn())
        return;

    if (_triggerType == ON_TRIGGER_ENTER && _triggerComponent != nullptr)
    {
        if (OtherActor && (OtherActor != this))
        {
            _triggerComponent->TriggerQuest();
        }
    }
}

void AQuestTrigger::OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor)
{
    if (OtherActor != GetWorld()->GetFirstPlayerController()->GetPawn())
        return;

    if (_triggerType == ON_TRIGGER_EXIT && _triggerComponent != nullptr)
    {
        if (OtherActor && (OtherActor != this))
        {
            _triggerComponent->TriggerQuest();
        }
    }
}


