// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerUtilityHelper.h"

// Sets default values
APlayerUtilityHelper::APlayerUtilityHelper()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}




void APlayerUtilityHelper::FindAllTimePeriodActors()
{
	for (int i = 0; i < _timePeriodTags.Num(); i++)
	{
		TArray<AActor*> _foundActors;
		UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName(_timePeriodTags[i]._tag), _foundActors);
		
		for (int j = 0; j < _foundActors.Num(); j++)
		{
			_timePeriodActors.AddUnique(FTimePeriodActor(_foundActors[j], _timePeriodTags[i]._timePeriod));
		}

	}
}


// Called when the game starts or when spawned
void APlayerUtilityHelper::BeginPlay()
{
	Super::BeginPlay();

	APawn* Player = GetWorld()->GetFirstPlayerController()->GetPawn();

	FindAllTimePeriodActors();


	if (Player != nullptr)
	{
		if (UPlayerUtilities* playerUtils = (UPlayerUtilities*)Player->GetComponentByClass(UPlayerUtilities::StaticClass()))
		{
			playerUtils->_timePeriodActors = _timePeriodActors;
		}
	}
	
}

// Called every frame
void APlayerUtilityHelper::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

