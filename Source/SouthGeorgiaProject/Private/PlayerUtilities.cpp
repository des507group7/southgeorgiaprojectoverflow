// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerUtilities.h"
#include "PlayerJournalManager.h"

// Sets default values for this component's properties
UPlayerUtilities::UPlayerUtilities()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UPlayerUtilities::BeginPlay()
{
	Super::BeginPlay();

	// ...
	_hasTraveledBack = false;

	GetWorld()->GetFirstPlayerController()->InputComponent->BindAction("TestTimeTravel", EInputEvent::IE_Pressed, this, &UPlayerUtilities::StartTimeTravel);
	UpdateTimePeriodActors();

}


// Called every frame
void UPlayerUtilities::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...

	OnTimeTravel.Broadcast();

}

void UPlayerUtilities::StartTimeTravel()
{
	_isTimeTravelling = true;

	if (_timePeriod == 0)
		_timePeriod = 2;
	else
		_timePeriod = 0;

	UpdateTimePeriodActors();
}

void UPlayerUtilities::StartTimeTravelWithParam(int timePeriod)
{
	_isTimeTravelling = true;
	_timePeriod = timePeriod;
	UpdateTimePeriodActors();
}


void UPlayerUtilities::UpdateTimePeriodActors()
{
	//Loop through the time period actors
	for (int i = 0; i < _timePeriodActors.Num(); i++)
	{
		//if their time period matches the current one then show and enable collisions
		//otherwise hide and disable collisions
		if (_timePeriodActors[i]._timePeriod == _timePeriod)
		{
			if (_timePeriodActors[i]._actor != nullptr)
			{
				_timePeriodActors[i]._actor->SetActorHiddenInGame(false);
				_timePeriodActors[i]._actor->SetActorEnableCollision(true);
			}
		}
		else
		{
			if (_timePeriodActors[i]._actor != nullptr)
			{
				_timePeriodActors[i]._actor->SetActorHiddenInGame(true);
				_timePeriodActors[i]._actor->SetActorEnableCollision(false);
			}
		}
	}
}

void UPlayerUtilities::StopTimeTravel()
{
	_isTimeTravelling = false;

	if (!_hasTraveledBack && _timePeriod == 0)
	{
		if (UPlayerJournalManager* journal = (UPlayerJournalManager*)GetOwner()->GetComponentByClass(UPlayerJournalManager::StaticClass()))
		{
			if(journal->_spiritIntroDialogue != nullptr)
				journal->_spiritIntroDialogue->SetAsCurrentDialogue();
		}
		_hasTraveledBack = true;
	}
}

void UPlayerUtilities::OnLoad(int timePeriod)
{
	StartTimeTravelWithParam(timePeriod);
}

