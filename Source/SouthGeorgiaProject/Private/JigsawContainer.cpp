// Fill out your copyright notice in the Description page of Project Settings.


#include "JigsawContainer.h"
#include "JigsawManager.h"

// Sets default values for this component's properties
UJigsawContainer::UJigsawContainer()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UJigsawContainer::BeginPlay()
{
	Super::BeginPlay();

	_isCompleted = false;
}

// Called every frame
void UJigsawContainer::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

//This function begins the puzzle stored in this container
void UJigsawContainer::StartPuzzle()
{
	if (_isCompleted)
		return;

	//Get the jigsaw manager component from the player pawn
	if (UJigsawManager* playerJigsaw = UGameplayStatics::GetPlayerPawn(GetWorld(), 0)->FindComponentByClass<UJigsawManager>())
	{
		//begin the puzzle in the player jigsaw manager using this container's stored puzzle

		UDialogueContainer* dialogue = (UDialogueContainer*)GetOwner()->GetComponentByClass(UDialogueContainer::StaticClass());
		UQuestTriggerComponent* quest = (UQuestTriggerComponent*)GetOwner()->GetComponentByClass(UQuestTriggerComponent::StaticClass());

		if (_puzzle == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("No Puzzzle Assigned"));
		}
		else
		{
			playerJigsaw->UpdateCurrentPuzzle(_puzzle, dialogue, quest, this);
			playerJigsaw->BeginPuzzle();
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Unable to find player jigsaw manager component"));
	}
}


