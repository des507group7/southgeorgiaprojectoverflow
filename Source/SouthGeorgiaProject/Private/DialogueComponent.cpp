// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueComponent.h"

#define print(text) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Green,text)
#define printFString(text, fstring) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT(text), fstring))

// Sets default values for this component's properties
UDialogueComponent::UDialogueComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	//Init the speaker and subtitle strings
	_currentSpeaker = "";
	_currentSubtitles = "";
	// ...

	static ConstructorHelpers::FClassFinder<UDialogueUI> DialogueUI(TEXT("WidgetBlueprint'/Game/UI/DialogueSubtitlesBP.DialogueSubtitlesBP_C'"));

	if (DialogueUI.Class != nullptr)
	{
		_dialogueUIClass = DialogueUI.Class;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to Find Dialogue UI Class"));
	}		
}


// Called when the game starts
void UDialogueComponent::BeginPlay()
{
	Super::BeginPlay();

	//Init the speaker and subtitle strings
	_currentSpeaker = "";
	_currentSubtitles = "";

	//Get the audio source component from this object
	_dialogueAudioSource = GetOwner()->FindComponentByClass<UAudioComponent>();
	_dialogueFMODAudioSource = GetOwner()->FindComponentByClass<UFMODAudioComponent>();


	if (_dialogueUIClass != nullptr)
	{
		_dialogueUI = CreateWidget<UDialogueUI>(GetWorld()->GetFirstPlayerController(), _dialogueUIClass);

		if (_dialogueUI != nullptr)
		{
			_dialogueUI->AddToViewport();

			_dialogueUI->ToggleVisible(false);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Failed to create Objective UI widget"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to add Dialogue UI to viewport"));
	}
}


// Called every frame
void UDialogueComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//GetOwner()->InputComponent->BindAction("StartDialogue", IE_Released, this, &UDialogueComponent::BeginDialogue);
}

//This function will be called upon a dialogue event being triggered
void UDialogueComponent::BeginDialogue()
{

	//Error handling for ensuring there is dialogue array to read from
	if (_currentDialogue.Num() <= 0)
		return;
	


	//Don't proceed if there is already dialogue in progress
	if (!_isDialogueInProgress)
	{
		_isDialogueInProgress = true;
		//Set the current index to the first element
		_currentDialogueBeat = 0;

		if(_dialogueUI != nullptr)
			_dialogueUI->ToggleVisible(true);

		//Begin the display
		DisplayBeat();
	}
}

//This function returns the current subtitles
//Used by the UI widget
FString UDialogueComponent::GetCurrentSubtitle()
{
	return _currentSubtitles.IsEmpty() ? FString("") : _currentSubtitles;
}

//This function is responsible for displaying the current dialogue beat
void UDialogueComponent::DisplayBeat()
{


	//Error handling for ensuring there is dialogue array to read from
	if (_currentDialogue.Num() <= 0)
		return;

	//Store the subtitles and speaker name for the current dialogue beat
	//These values are read from the UI widget blueprint and assigned where appropriate there.
	_currentSpeaker = _currentDialogue[_currentDialogueBeat]._speakerName;
	_currentSubtitles = _currentDialogue[_currentDialogueBeat]._subtitles;


	if (_dialogueUI != nullptr)
		_dialogueUI->UpdateUI(_currentSpeaker, _currentSubtitles);

	//Access the FMOD Event for the current dialogue beat
	UFMODEvent* currentFMODEvent = _currentDialogue[_currentDialogueBeat]._audioEvent;
	UFMODEvent* currentFMODAmbience = _currentDialogue[_currentDialogueBeat]._ambience;
	//Access the audio clip for the current dialogue beat
	USoundCue* currentAudioClip = _currentDialogue[_currentDialogueBeat]._audio;
	//Store the audio clip length to determine how long the beat should exist for
	//A default value is assigned before checking the clip is valid to ensure the subtitles remain for an appropriate amount of time
	//where no clip is present, and prevents any errors trying to access an invalid clip's duration
	float audioLength = 10.0f;


	if (currentFMODEvent)
	{
		if (_dialogueFMODAudioSource != nullptr)
		{
			_dialogueFMODAudioSource->SetEvent(currentFMODEvent);
			audioLength = ((float)(_dialogueFMODAudioSource->GetLength())/1000);
			_dialogueFMODAudioSource->Play();
		}
	}
	else if (currentAudioClip != nullptr)
	{
		//If an audio component is present on this actor and a valid audio clip for the current beat is found
		if (_dialogueAudioSource != nullptr)
		{
			//Update the audio length variable
			audioLength = currentAudioClip->Duration;
			//Assign the clip to the audio component
			_dialogueAudioSource->Sound = currentAudioClip;
			//Begin playing the audio for this beat
			_dialogueAudioSource->Play();
		}
	}

	if (currentFMODAmbience)
	{
		_currentAmbience = UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), currentFMODAmbience, GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorTransform(), true);
	}
	

	




	FTimerHandle handle; //Storage not needed, therefore can be declared within this function's scope
	FTimerManager* tm = &GetWorld()->GetTimerManager(); //Used to ensure the timer manager is valid

	if (tm != nullptr)
	{
		//The delay before moving onto the next beat
		float delay = 0.0f;
		//Determine the delay length based on the beat's progression type and if the audio clip is valid
		//Either the audio clip length will be used or the manually set value

		if(currentFMODEvent != nullptr || currentAudioClip != nullptr)
			delay = _currentDialogue[_currentDialogueBeat]._progressionType == AudioClipLength ? audioLength + _currentDialogue[_currentDialogueBeat]._delayTime : _currentDialogue[_currentDialogueBeat]._delayTime;
		else
			delay = _currentDialogue[_currentDialogueBeat]._delayTime;


		//Call the next beat function after the delay time has elapsed
		if (delay <= 0.0f)
			NextBeat();
		else
			tm->SetTimer(handle, this, &UDialogueComponent::NextBeat, delay, true);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Timer Manager Invalid"));
	}
}



//This function is used to progress the dialogue onto the next beat
void UDialogueComponent::NextBeat()
{
	//Error handling for ensuring there is dialogue array to read from
	if (_currentDialogue.Num() <= 0)
		return;

	//Clear any outstanding timers
	//TODO Convert previous timer to store handle so only that specific timer can be stopped/restarted
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);

	if (_currentAmbience.Instance != nullptr)
	{
		_currentAmbience.Instance->stop(FMOD_STUDIO_STOP_MODE::FMOD_STUDIO_STOP_IMMEDIATE);
	}

	if (_dialogueFMODAudioSource != nullptr)
	{
		if (_dialogueFMODAudioSource->IsPlaying())
			_dialogueFMODAudioSource->Stop();
	}

	//Stop the audio source playing if the component is valid
	if (_dialogueAudioSource != nullptr)
	{
		if (_dialogueAudioSource->IsPlaying())
			_dialogueAudioSource->Stop();
	}

	//Increment the beat counter
	_currentDialogueBeat++;

	//If past the length of the dialogue then finish, otherwise display the next beat
	if (_currentDialogueBeat >= _currentDialogue.Num())
		FinishDialogue();
	else
		DisplayBeat();
}

//This function is called when there are no other beats to display in the current dialogue
void UDialogueComponent::FinishDialogue()
{

	if (_dialogueUI != nullptr)
		_dialogueUI->ToggleVisible(false);

	//Cleanup Text UI
	_currentSubtitles = "";
	_currentSpeaker = "";
	//Set flag to false
	_isDialogueInProgress = false;

	if (_dialogueFMODAudioSource != nullptr)
	{
		if (_dialogueFMODAudioSource->IsPlaying())
			_dialogueFMODAudioSource->Stop();
	}

	//Stop any audio clips currently playing
	if (_dialogueAudioSource != nullptr)
	{
		if(_dialogueAudioSource->IsPlaying())
			_dialogueAudioSource->Stop();
	}
	//Empty the array
	_currentDialogue.Empty();
}

//This function returns whether there is a dialogue in progress
//Used to determine whether the UI widget should be visible
bool UDialogueComponent::IsInProgress()
{
	return _isDialogueInProgress;
}

//This function is called from a dialogue container and is used to set the dialogue beats array
void UDialogueComponent::SetCurrentDialogueData(TArray<FDialogueBeat> dialogue)
{
	//Remove existing data
	_currentDialogue.Empty();
	//Store the new data
	_currentDialogue = dialogue;
	//Overwrite progress flag from previous encounter
	_isDialogueInProgress = false;
}

//This function returns the current speaker name.
//Used by the UI widget blueprint
FString UDialogueComponent::GetCurrentSpeakerName()
{
	return _currentSpeaker.IsEmpty() ? FString("") : _currentSpeaker;

}
