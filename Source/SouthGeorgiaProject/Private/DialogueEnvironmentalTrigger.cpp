// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueEnvironmentalTrigger.h"
#include "Components/BillboardComponent.h"

ADialogueEnvironmentalTrigger::ADialogueEnvironmentalTrigger()
{

	//Register Events
	OnActorBeginOverlap.AddDynamic(this, &ADialogueEnvironmentalTrigger::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &ADialogueEnvironmentalTrigger::OnOverlapEnd);

	_dialogueContainer = CreateDefaultSubobject<UDialogueContainer>(TEXT("Dialogue Container"));
}

void ADialogueEnvironmentalTrigger::BeginPlay()
{
	if(_drawDebugLines)
		DrawDebugBox(GetWorld(), GetActorLocation(), GetComponentsBoundingBox().GetExtent(), FColor::Purple, true, -1, 0, 5);
}

void ADialogueEnvironmentalTrigger::OnOverlapBegin(AActor* OverlappedActor, AActor* OtherActor)
{
	if (OtherActor == (AActor*)GetWorld()->GetFirstPlayerController()->GetPawn())
	{
		if (!_hasBeenTriggered && _dialogueContainer != nullptr)
		{
			_dialogueContainer->SetAsCurrentDialogue();
			_hasBeenTriggered = true;
		}
	}
}

void ADialogueEnvironmentalTrigger::OnOverlapEnd(AActor* OverlappedActor, AActor* OtherActor)
{
}
