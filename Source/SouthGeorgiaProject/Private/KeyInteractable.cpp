// Fill out your copyright notice in the Description page of Project Settings.


#include "KeyInteractable.h"

AKeyInteractable::AKeyInteractable()
{
	_myCollider = CreateDefaultSubobject<UBoxComponent>(FName("Collision Mesh"));
	_myCollider->SetCollisionProfileName("BlockAllDynamic");
	SetRootComponent(_myCollider);
}

void AKeyInteractable::Interact()
{
	if (_linkedDoor != nullptr)
		_linkedDoor->Unlock();

	if (_keyPickupSound != nullptr)
		UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), _keyPickupSound, GetActorTransform(), true);


	Destroy();
}