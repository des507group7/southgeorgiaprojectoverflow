// Fill out your copyright notice in the Description page of Project Settings.


#include "NoteInteractable.h"

ANoteInteractable::ANoteInteractable()
{
	_dialogueContainer = CreateDefaultSubobject<UDialogueContainer>(TEXT("Dialogue Container"));

	_myCollider = CreateDefaultSubobject<UBoxComponent>(FName("Collision Mesh"));
	_myCollider->SetCollisionProfileName("BlockAllDynamic");
	SetRootComponent(_myCollider);

}

void ANoteInteractable::Interact()
{
	Super::Interact();

	if (_notePickupSound != nullptr)
		UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), _notePickupSound, GetActorTransform(), true);
}
