// Fill out your copyright notice in the Description page of Project Settings.


#include "SaveGameHelper.h"

// Sets default values
ASaveGameHelper::ASaveGameHelper()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASaveGameHelper::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void ASaveGameHelper::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

TArray<FDoorSaveData> ASaveGameHelper::GetDoorData()
{
	TArray<FDoorSaveData> _doors;
	_doors.SetNum(_allDoors.Num());

	for (int i = 0; i < _allDoors.Num(); i++)
	{
		_doors[i]._door = _allDoors[i];
		_doors[i]._locked = _allDoors[i]->_isLocked;
		_doors[i]._open = _allDoors[i]->_isOpen;
	}

	return _doors;
}


void ASaveGameHelper::SetDoorData(FDoorSaveData door)
{
	for (int i = 0; i < _allDoors.Num(); i++)
	{
		if (_allDoors[i] == door._door)
		{

			if(_allDoors[i]->_isOpen != door._open)
				_allDoors[i]->Interact();

			if (door._locked && _allDoors[i]->_isOpen)
			{
				_allDoors[i]->_isLocked = false;
				_allDoors[i]->Interact();
			}

			_allDoors[i]->_isLocked = door._locked;

			return;
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("Failed to find Door"));
}