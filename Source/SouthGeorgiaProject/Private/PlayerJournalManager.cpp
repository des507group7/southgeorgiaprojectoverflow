// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerJournalManager.h"
#include "TimeTravelTrigger.h"

// Sets default values for this component's properties
UPlayerJournalManager::UPlayerJournalManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UPlayerJournalManager::BeginPlay()
{
	Super::BeginPlay();

	// ...
	_playerutils = (UPlayerUtilities*)GetOwner()->GetComponentByClass(UPlayerUtilities::StaticClass());
}


// Called every frame
void UPlayerJournalManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UPlayerJournalManager::ShowJournal()
{
	if (_journal != nullptr)
	{
		_journal->AddToViewport();
		_isReadingJournal = true;
		if(_playerutils != nullptr)
			_playerutils->_canControlPlayer = false;
	}
}

void UPlayerJournalManager::HideJournal()
{
	if (_journal != nullptr)
	{
		_journal->RemoveFromViewport();
		_isReadingJournal = false;
		if (_playerutils != nullptr)
			_playerutils->_canControlPlayer = true;
	}

	if (!_hasClosedJournalForFirstTime)
	{
		_hasClosedJournalForFirstTime = true;

		if (UTimeTravelTrigger* trigger = (UTimeTravelTrigger*)_journalInteractable->GetComponentByClass(UTimeTravelTrigger::StaticClass()))
		{
			trigger->TriggerTimeTravel();
		}
	}
}

void UPlayerJournalManager::PickupJournal(UUserWidget* widget, AActor* interactable)
{
	if (interactable != nullptr)
		_journalInteractable = interactable;

	if (widget != nullptr)
	{
		_journal = widget;
		_hasJournal = true;
		_gameStage = 0;
		ShowJournal();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Unable to pickup Journal. No Widget Assigned."));
	}
}

