// Fill out your copyright notice in the Description page of Project Settings.


#include "JigsawPiece.h"
#include "Layout/Geometry.h"
#include <Runtime/UMG/Public/Components/CanvasPanelSlot.h>
#include "UObject/ConstructorHelpers.h"
#include "PuzzlePiecePreview.h"
#include "Jigsaw.h"

UJigsawPiece::UJigsawPiece(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

	static ConstructorHelpers::FClassFinder<UPuzzlePiecePreview> WidgetPreview(TEXT("WidgetBlueprint'/Game/BluePrints/Jigsaw/JigsawUtility/PuzzlePreview.PuzzlePreview_C'"));

	if (WidgetPreview.Class != nullptr)
	{
		_widgetDrag = WidgetPreview.Class;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to Find Widget Preview Class"));
	}
}

void UJigsawPiece::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	UCanvasPanelSlot* _mySlot = Cast<UCanvasPanelSlot>(Slot);
	if (_mySlot != nullptr)
	{
		UpdateStartingPos(GetCachedGeometry().AbsoluteToLocal(_mySlot->GetPosition()));
	}

	_pieceSize = GetSize();

	_isCorrectlyPlaced = false;

	
}
FVector2D UJigsawPiece::GetSize()
{
	UCanvasPanelSlot* _mySlot = Cast<UCanvasPanelSlot>(Slot);
	if (_mySlot != nullptr)
	{
		return _mySlot->GetSize();
	}

	return FVector2D(100, 100);
}

void UJigsawPiece::SetSize(FVector2D size)
{
	UCanvasPanelSlot* _mySlot = Cast<UCanvasPanelSlot>(Slot);
	if (_mySlot != nullptr)
	{
		_mySlot->SetSize(size);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to set puzzle piece size"));
	}
	PullToFront();
}

void UJigsawPiece::PullToFront()
{
	UCanvasPanelSlot* _mySlot = Cast<UCanvasPanelSlot>(Slot);
	if (_mySlot != nullptr)
	{
		_mySlot->SetZOrder(1);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to move puzzle piece to front"));
	}
}

void UJigsawPiece::PushToBack()
{
	UCanvasPanelSlot* _mySlot = Cast<UCanvasPanelSlot>(Slot);

	if (_mySlot != nullptr)
	{
		_mySlot->SetZOrder(-1);
	}
	else
	{
	//	UE_LOG(LogTemp, Warning, TEXT("Failed to move puzzle piece to back"));
	}
}

void UJigsawPiece::NativeConstruct()
{
	Super::NativeConstruct();
}

void UJigsawPiece::NativePreConstruct()
{
	Super::NativePreConstruct();
	

	if (PuzzlePiece != nullptr)
	{
		PuzzlePiece->SetBrush(_puzzlePieceImage);
		

		UCanvasPanelSlot* _mySlot = Cast<UCanvasPanelSlot>(Slot);
		if (_mySlot != nullptr)
		{
			_mySlot->SetSize(_puzzlePieceImage.GetImageSize());
		}
	}
}

FReply UJigsawPiece::NativeOnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& InMouseEvent)
{
	FReply reply = Super::NativeOnMouseButtonDown(MyGeometry, InMouseEvent);

	if (!_isCorrectlyPlaced)
	{
		_dragOffset = MyGeometry.AbsoluteToLocal(InMouseEvent.GetScreenSpacePosition());

		if (InMouseEvent.GetEffectingButton() == EKeys::LeftMouseButton)
		{
			reply.DetectDrag(TakeWidget(), EKeys::LeftMouseButton);

			if (_pickedUpPiece != nullptr)
				UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), _pickedUpPiece, GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorTransform(), true);

		}
	}
	return reply;
}


void UJigsawPiece::NativeOnDragDetected(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent, UDragDropOperation*& OutOperation)
{
	Super::NativeOnDragDetected(InGeometry, InMouseEvent, OutOperation);

	if (_widgetDrag != nullptr)
	{
		_draggedWidget = CreateWidget(GetWorld()->GetFirstPlayerController(), _widgetDrag);
		UPuzzlePiecePreview* preview = Cast<UPuzzlePiecePreview>(_draggedWidget);
		if (preview)
			preview->_widgetReference = this;
		else
			UE_LOG(LogTemp, Warning, TEXT("No Preview Detected"));


		auto DragDropOperation = NewObject<UJigsawDrag>();

		DragDropOperation->DefaultDragVisual = _draggedWidget;
		DragDropOperation->Pivot = EDragPivot::MouseDown;
		DragDropOperation->_widgetReference = this;
		DragDropOperation->_dragOffset = _dragOffset;

		DragDropOperation->_widgetSize = _pieceSize;

		OutOperation = DragDropOperation;
		RemoveFromViewport();

	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No widgetDrag found"));
	}
}

void UJigsawPiece::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (_isCorrectlyPlaced && PuzzlePiece->ColorAndOpacity.A > 0.0f && _puzzleCompleted)
	{
		PuzzlePiece->SetColorAndOpacity(FLinearColor(PuzzlePiece->ColorAndOpacity.R, PuzzlePiece->ColorAndOpacity.G, PuzzlePiece->ColorAndOpacity.B, PuzzlePiece->ColorAndOpacity.A - InDeltaTime));
	}
}

void UJigsawPiece::DebugLog(FString output)
{
	UE_LOG(LogTemp, Warning, TEXT("%s"), *output);
}


void UJigsawPiece::UpdateStartingPos(FVector2D startingPos)
{
	FString name = GetName();
	_targetPosition = startingPos;
}

void UJigsawPiece::UpdateCurrentPos(FVector2D current)
{
	_currentPosition = current;
	CheckForCorrectPos();
}

void UJigsawPiece::CheckForCorrectPos()
{
	//The distance between the current and target positions of the puzzle piece
	float distance = FVector2D::Distance(_currentPosition, _targetPosition);
	//UE_LOG(LogTemp, Warning, TEXT("%f"), distance);


	if (distance < 50.0f)
	{
		SetPositionInViewport(_targetPosition, false);
		_isCorrectlyPlaced = true;
		PushToBack();

		if (_correctlyPlacedPiece != nullptr)
			UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), _correctlyPlacedPiece, GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorTransform(), true);

	}
}

void UJigsawPiece::SetPosition(FVector2D newPosition)
{
	UCanvasPanelSlot* _mySlot = Cast<UCanvasPanelSlot>(Slot);
	_mySlot->SetPosition(newPosition);
	UpdateCurrentPos(newPosition);
}

bool UJigsawPiece::NativeOnDrop(const FGeometry& MyGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* Operation)
{
	Super::NativeOnDrop(MyGeometry, InDragDropEvent, Operation);

	//Manually trigger the on drop of the current drag/drop on the parent jigsaw widget instead of on this widget
	if (UJigsaw* jigsaw = Cast<UJigsaw>(_myParent))
	{
		jigsaw->NativeOnDrop(MyGeometry, InDragDropEvent, Operation);
	}
	return true;
}