// Fill out your copyright notice in the Description page of Project Settings.


#include "MySaveGame.h"

UMySaveGame::UMySaveGame()
{
	_playerPosition = FVector(0.0f, 0.0f, 0.0f);
	_playerRotation = FRotator(0.0f, 0.0f, 0.0f);
	_currentObjectiveIndex = 0;
	_currentTimePeriodIndex = 0;
}