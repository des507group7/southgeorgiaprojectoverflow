// Fill out your copyright notice in the Description page of Project Settings.


#include "PuzzlePiecePreview.h"

void UPuzzlePiecePreview::NativeConstruct()
{
	Super::NativeConstruct();

	if (_widgetReference != nullptr)
	{
		UJigsawPiece* puzzle = Cast<UJigsawPiece>(_widgetReference);
		
		if (puzzle)
		{
			FVector2D pieceSize = puzzle->_pieceSize;
			
			WidgetSize->SetWidthOverride(pieceSize.X);
			WidgetSize->SetHeightOverride(pieceSize.Y);
		}

		if (PreviewImage != nullptr)
		{
			PreviewImage->SetBrush(puzzle->_puzzlePieceImage);
			PreviewImage->SetColorAndOpacity(FLinearColor(0.0f, 0.0f, 0.0f, 0.5f));
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No Preview Image Found on Drag Preview"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Widget Reference Invalid on Preview"));
	}
}
