// Fill out your copyright notice in the Description page of Project Settings.


#include "GameStateTrigger.h"

// Sets default values for this component's properties
UGameStateTrigger::UGameStateTrigger()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGameStateTrigger::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UGameStateTrigger::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UGameStateTrigger::TriggerGameState()
{
	if (AActor* player = GetWorld()->GetFirstPlayerController()->GetPawn())
	{
		if (UPlayerJournalManager* journal = (UPlayerJournalManager*)player->GetComponentByClass(UPlayerJournalManager::StaticClass()))
		{
			journal->_gameStage = _targetGameStage;
		}
	}
}
