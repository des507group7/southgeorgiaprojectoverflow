// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MySaveGame.h"
#include "DoorInteractable.h"
#include "SaveGameHelper.generated.h"

UCLASS()
class SOUTHGEORGIAPROJECT_API ASaveGameHelper : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASaveGameHelper();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		TArray<ADoorInteractable*> _allDoors;

	UFUNCTION()
		TArray<FDoorSaveData> GetDoorData();

	UFUNCTION()
		void SetDoorData(FDoorSaveData door);
};
