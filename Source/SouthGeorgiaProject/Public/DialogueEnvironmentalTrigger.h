// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "DialogueContainer.h"
#include "DrawDebugHelpers.h"
#include "DialogueEnvironmentalTrigger.generated.h"

/**
 * 
 */
UCLASS()
class SOUTHGEORGIAPROJECT_API ADialogueEnvironmentalTrigger : public ATriggerBox
{
	GENERATED_BODY()
public:
	ADialogueEnvironmentalTrigger();

	UFUNCTION()
		void OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor);

	UFUNCTION()
		void OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor);

	UPROPERTY(EditAnywhere)
		UDialogueContainer* _dialogueContainer = nullptr;
	UPROPERTY(EditAnywhere)
		bool _drawDebugLines = false;

private:
	bool _hasBeenTriggered = false;

protected:
	virtual void BeginPlay() override;
};
