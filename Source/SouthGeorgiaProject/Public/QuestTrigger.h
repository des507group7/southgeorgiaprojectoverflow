// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QuestManager.h"
#include "Engine/TriggerBox.h"
#include "DrawDebugHelpers.h"
#include "QuestTriggerComponent.h"
#include "GameFramework/Actor.h"
#include "QuestTrigger.generated.h"

UENUM()
	enum QuestTriggerType { ON_TRIGGER_ENTER, ON_TRIGGER_EXIT };

UCLASS()
class SOUTHGEORGIAPROJECT_API AQuestTrigger : public ATriggerBox
{
	GENERATED_BODY()

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Sets default values for this component's properties
	AQuestTrigger();
	

	// declare overlap begin function
	UFUNCTION()
		void OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor);

	// declare overlap end function
	UFUNCTION()
		void OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor);

	UPROPERTY(EditAnywhere)
		TEnumAsByte<QuestTriggerType> _triggerType = ON_TRIGGER_ENTER;

	UPROPERTY(EditAnywhere)
		UQuestTriggerComponent* _triggerComponent;

	UPROPERTY(EditAnywhere)
		bool _drawDebugLines = false;
};
