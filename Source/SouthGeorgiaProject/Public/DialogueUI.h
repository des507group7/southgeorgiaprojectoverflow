// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "DialogueUI.generated.h"

/**
 * 
 */
UCLASS()
class SOUTHGEORGIAPROJECT_API UDialogueUI : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(meta = (BindWidget))
		UTextBlock* SpeakerName;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* Subtitle;

	UFUNCTION()
		void UpdateUI(FString speaker, FString contents);

	UFUNCTION()
		void ToggleVisible(bool visible);
};
