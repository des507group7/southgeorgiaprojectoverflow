// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Sound/SoundCue.h"
#include "Components/AudioComponent.h"
#include "DialogueUI.h"
#include "FMODEvent.h"
#include "FMODBlueprintStatics.h"
#include "FMODAudioComponent.h"
#include "DialogueComponent.generated.h"




UENUM()
enum ProgressionType { AudioClipLength, ManualLength };

USTRUCT(BlueprintType)
struct FDialogueBeat
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString _speakerName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString _subtitles;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USoundCue* _audio;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UFMODEvent* _audioEvent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UFMODEvent* _ambience;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<ProgressionType> _progressionType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float _delayTime;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SOUTHGEORGIAPROJECT_API UDialogueComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDialogueComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
		void BeginDialogue();
	UFUNCTION()
		void NextBeat();
	UFUNCTION()
		void FinishDialogue();
	UFUNCTION()
		void DisplayBeat();
	
	UFUNCTION(BlueprintCallable)
		FString GetCurrentSubtitle();

	UFUNCTION(BlueprintCallable)
		bool IsInProgress();

	UFUNCTION(BlueprintCallable)
		void SetCurrentDialogueData(TArray<FDialogueBeat> dialogue);
	

	UFUNCTION(BlueprintCallable)
		FString GetCurrentSpeakerName();


	FFMODEventInstance _currentAmbience;

	int _currentDialogueBeat = 0;
	bool _isDialogueInProgress = false;

	UPROPERTY()
		TArray<FDialogueBeat> _currentDialogue;

	UAudioComponent* _dialogueAudioSource;
	UFMODAudioComponent* _dialogueFMODAudioSource;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Dialogue)
		FString _currentSubtitles;

	FString _currentSpeaker;

	UDialogueUI* _dialogueUI;
	TSubclassOf<class UDialogueUI> _dialogueUIClass;
};
