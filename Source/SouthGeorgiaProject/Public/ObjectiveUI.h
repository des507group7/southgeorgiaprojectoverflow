// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "ObjectiveUI.generated.h"

/**
 * 
 */
UCLASS()
class SOUTHGEORGIAPROJECT_API UObjectiveUI : public UUserWidget
{
	GENERATED_BODY()
	
public :
	UPROPERTY(meta = (BindWidget))
		UTextBlock* Objective;

	void UpdateObjective(FString obj);
};
