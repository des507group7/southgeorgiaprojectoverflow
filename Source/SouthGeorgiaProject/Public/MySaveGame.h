// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "DoorInteractable.h"
#include "MySaveGame.generated.h"

/**
 * 
 */

USTRUCT(BlueprintType)
struct FDoorSaveData
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AActor* _door;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool _open;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool _locked;
};

UCLASS()
class SOUTHGEORGIAPROJECT_API UMySaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:
	UMySaveGame();

	//This is where the savable properties are declared
	UPROPERTY(EditAnywhere)
		FVector _playerPosition;
	UPROPERTY(EditAnywhere)
		FRotator _playerRotation;
	UPROPERTY(EditAnywhere)
		int _currentObjectiveIndex;
	UPROPERTY(EditAnywhere)
		int _currentTimePeriodIndex;
	UPROPERTY(EditAnywhere)
		TArray<FDoorSaveData> _doors;

};
