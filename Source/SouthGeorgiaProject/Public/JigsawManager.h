// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Blueprint/UserWidget.h"
#include "Jigsaw.h"
#include "DialogueContainer.h"
#include "QuestTriggerComponent.h"
#include "JigsawContainer.h"
#include "PlayerUtilities.h"
#include "JigsawManager.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SOUTHGEORGIAPROJECT_API UJigsawManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UJigsawManager();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, Category = JigsawManager)
		bool _isInPuzzle = false; //Tracks whether a puzzle is in progress
	
	UPROPERTY()
		TSubclassOf<UJigsaw> _currentPuzzleClass; //The current puzzle's class
	
	UPROPERTY()
		UJigsaw* _currentPuzzle; //The current puzzle's widget




	//UFUNCTION()
		//void UpdateCurrentPuzzle(TSubclassOf<UJigsaw> newPuzzle);

	//void UpdateCurrentPuzzle(TSubclassOf<UJigsaw> newPuzzle, UDialogueContainer* dialogue);
	void UpdateCurrentPuzzle(TSubclassOf<UJigsaw> newPuzzle, UDialogueContainer* dialogue = nullptr, UQuestTriggerComponent* questTrigger = nullptr, UJigsawContainer* container = nullptr);

	UFUNCTION()
		void BeginPuzzle();

	UFUNCTION()
		void FinishPuzzle();

	UFUNCTION(BlueprintCallable)
		bool GetPuzzleStatus();

	UJigsawContainer* _jigsawContainer;
	UDialogueContainer* _dialogue;
	UQuestTriggerComponent* _questTrigger;
	UPlayerUtilities* _playerUtils;
};
