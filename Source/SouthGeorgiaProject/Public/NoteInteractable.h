// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "NoteInteractable.generated.h"

/**
 * 
 */
UCLASS()
class SOUTHGEORGIAPROJECT_API ANoteInteractable : public AInteractable
{
	GENERATED_BODY()
public:
	ANoteInteractable();
	virtual void Interact() override;

	UBoxComponent* _myCollider = nullptr;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UFMODEvent* _notePickupSound;
};
