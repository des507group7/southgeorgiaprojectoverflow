// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "FMODEvent.h"
#include "FMODBlueprintStatics.h"
#include "FMODAudioComponent.h"
#include "DialogueContainer.h"
#include "QuestTriggerComponent.h"
#include "Interactable.generated.h"

UCLASS()
class SOUTHGEORGIAPROJECT_API AInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractable();
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void BlueprintInteract();

	//UBoxComponent* _myCollider = nullptr;

	UPROPERTY(EditAnywhere)
		FString _prompt = "";

	UPROPERTY(EditAnywhere)
		UDialogueContainer* _dialogueContainer = nullptr;
	
	UQuestTriggerComponent* _questTrigger;
	bool _hasQuestBeenTriggered = false;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	
};
