// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "JigsawPiece.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/Image.h"
#include "Math/Vector.h"
#include "Layout/Geometry.h"
#include <Runtime/UMG/Public/Components/CanvasPanelSlot.h>
#include "FMODEvent.h"
#include "FMODBlueprintStatics.h"
#include "FMODAudioComponent.h"
#include "Jigsaw.generated.h"

/**
 * 
 */
UCLASS()
class SOUTHGEORGIAPROJECT_API UJigsaw : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeOnInitialized() override;
	virtual bool NativeOnDrop(const FGeometry & MyGeometry, const FDragDropEvent & InDragDropEvent, UDragDropOperation * Operation) override;
	virtual  void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	void CheckGameState();

	bool _isCompleted;

	void Cleanup();

	FTimerManager* _timerManager;
	TArray<UJigsawPiece*> _puzzlePieces;
	TArray<UWidget*> _puzzleTrays;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UImage* CompletedPuzzle; //The completed jigsaw image

	UFUNCTION()
		void Shuffle();
	FTimerHandle _completedTimerHandle;

	TArray<UJigsawPiece*> GetAllPuzzlePieces();


	UPROPERTY(EditAnywhere)
		UFMODEvent* _puzzleComplete;

};
