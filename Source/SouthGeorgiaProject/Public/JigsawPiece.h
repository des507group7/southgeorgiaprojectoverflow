// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Image.h"
#include "Math/Vector.h"
#include "JigsawDrag.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"
#include "FMODEvent.h"
#include "FMODBlueprintStatics.h"
#include "FMODAudioComponent.h"
#include "JigsawPiece.generated.h"

//This class represents the individual pieces that make up the jigsaw puzzles
//It will handle the game logic for determining it's position
//as well as the click and drag functionality for this piece
UCLASS()
class SOUTHGEORGIAPROJECT_API UJigsawPiece : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;
public:

	UJigsawPiece(const FObjectInitializer& ObjectInitializer); 

	UPROPERTY()
		FVector2D _currentPosition; //The current position of the piece, set after the drop operation
	UPROPERTY()
		FVector2D _targetPosition = FVector2D(0, 0); //The desired position for this piece, set on initialise
	UPROPERTY()
		FVector2D _dragOffset; //How far from the mouse pointer this widget will be during a drag
	UPROPERTY()
		UUserWidget* _draggedWidget; //The temporary widget that will follow the mouse pointer during a drag
	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UImage* PuzzlePiece; //The image used for this puzzle piece

	
	UPROPERTY(EditAnywhere, Category = Puzzle)
		FSlateBrush _puzzlePieceImage;

	virtual void NativePreConstruct() override;
	virtual FReply NativeOnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& InMouseEvent) override;
	virtual void NativeOnDragDetected(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent, UDragDropOperation*& OutOperation) override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	void DebugLog(FString output);
	
	TSubclassOf<UUserWidget> _widgetDrag; //Representative of the widget being dragged

	bool _isCorrectlyPlaced = false;

	UFUNCTION()
		virtual void NativeOnInitialized() override;

	UFUNCTION(BlueprintCallable)
		void UpdateStartingPos(FVector2D startingPos);

	UFUNCTION(BlueprintCallable)
		void UpdateCurrentPos(FVector2D current);

	UFUNCTION(BlueprintCallable)
		void CheckForCorrectPos();

	UFUNCTION()
		FVector2D GetSize();

	UFUNCTION()
		void SetSize(FVector2D size);

	UPROPERTY()
		FVector2D _pieceSize;

	bool _puzzleCompleted = false;

	void SetPosition(FVector2D newPosition);

	UFUNCTION()
		void PullToFront();
	UFUNCTION()
		void PushToBack();

	bool _pushedToBack = false;


	virtual bool NativeOnDrop(const FGeometry& MyGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* Operation) override;
	//class UJigsaw* _myParent;

	UUserWidget* _myParent;

	UPROPERTY(EditAnywhere, Category = Audio)
		UFMODEvent* _pickedUpPiece;

	UPROPERTY(EditAnywhere, Category = Audio)
		UFMODEvent* _correctlyPlacedPiece;

};
