// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "DoorInteractable.generated.h"

/**
 * 
 */
UCLASS()
class SOUTHGEORGIAPROJECT_API ADoorInteractable : public AInteractable
{
	GENERATED_BODY()
public:
	ADoorInteractable();

	UPROPERTY(EditAnywhere)
		FString _openPrompt;

	UPROPERTY(EditAnywhere)
		FString _closePrompt;

	UPROPERTY(EditAnywhere)
		FString _lockedPrompt;

	UPROPERTY(BlueprintReadWrite)
		bool _isOpen;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool _isLocked;

	virtual void Interact() override;

	UPROPERTY(BlueprintReadWrite)
		bool IsInteracting = false;

	UFUNCTION(BlueprintCallable)
		void SetOpen(bool open); 
	
	UFUNCTION(BlueprintCallable)
		bool GetOpen();

	virtual void BeginPlay() override;

	void Unlock();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UFMODEvent* _openSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UFMODEvent* _closeSound;

	UPROPERTY(BlueprintReadWrite)
		bool _interactFirstTime = false;
};
