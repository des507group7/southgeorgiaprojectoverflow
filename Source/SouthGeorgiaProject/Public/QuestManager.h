// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ObjectiveUI.h"
#include "QuestManager.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FQuestTriggerEvent, int, objectiveIndex);



USTRUCT(BlueprintType)
struct FObjectiveData
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString _objectiveDescription = "Objective Description";
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool _isComplete = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool _isMultiPart = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int _maxParts = 1;
		int _currentPart = 0;
};


UCLASS()// ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class SOUTHGEORGIAPROJECT_API UQuestManager : public UActorComponent
{
	GENERATED_BODY()



public:	
	// Sets default values for this component's properties
	UQuestManager();

	void BeginPlay();


	UFUNCTION(BlueprintCallable)
		void ProgressQuest();
	UFUNCTION(BlueprintCallable)
		void CompleteObjective(int objective);

	UFUNCTION(BlueprintCallable)
		FString GetCurrentObjective();

	UFUNCTION(BlueprintCallable)
		TArray<FObjectiveData> GetAllObjectiveData();


	bool IsIndexValid(int index);

	void OnLoad(int current);
	
	void AddNewObjective(FString desc);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
		FString _questName = "New Quest";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
		FString _questDescription = "New Quest Description";
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest, meta = (TitleProperty = "_objectiveDescription" ))
		TArray<FObjectiveData> _objectivesList;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
		bool _showObjectives = true;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Quest)
		int _currentObjective = 0;

	UPROPERTY(BlueprintAssignable, Category = QuestEvent)
		FQuestTriggerEvent _onQuestTrigger;

	UObjectiveUI* _objectiveUI;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
		TSubclassOf<class UObjectiveUI> _objectiveUIClass;
};
