// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "Blueprint/UserWidget.h"
#include "JournalInteractable.generated.h"

/**
 * 
 */
UCLASS()
class SOUTHGEORGIAPROJECT_API AJournalInteractable : public AInteractable
{
	GENERATED_BODY()
public:
	AJournalInteractable();
	virtual void Interact() override;


	UPROPERTY(EditAnywhere)
		UUserWidget* _journalUI;

	UBoxComponent* _myCollider = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UFMODEvent* _journalPickupSound;
};
