// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerUtilities.generated.h"


USTRUCT(BlueprintType)
struct FTimePeriodActor
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AActor* _actor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int _timePeriod;

	FTimePeriodActor()
	{

	}

	FTimePeriodActor(AActor* actor, int timePeriod)
	{
		_actor = actor;
		_timePeriod = timePeriod;
	}

	bool operator==(const FTimePeriodActor& arg1) const
	{
		return arg1._actor == _actor;
	}
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTimeTravel);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SOUTHGEORGIAPROJECT_API UPlayerUtilities : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerUtilities();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	//Variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerUtils)
		int _timePeriod = 3; //The current time period
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerUtils)
		bool _isTimeTravelling = false; //Determines whether a time travel operation is underway
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerUtils)
		bool _canControlPlayer = true; //Determines whether player input is enabled
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerUtils)
		TArray<FTimePeriodActor> _timePeriodActors; //List of all actors that are present in only one time period

	UFUNCTION(BlueprintCallable)
		void StartTimeTravel();
	
	UFUNCTION(BlueprintCallable)
		void StartTimeTravelWithParam(int timePeriod);

	UFUNCTION(BlueprintCallable)
		void StopTimeTravel();
	UFUNCTION()
		void UpdateTimePeriodActors();

	UFUNCTION()
		void OnLoad(int timePeriod);


	UPROPERTY(BlueprintAssignable)
		FOnTimeTravel OnTimeTravel;

	//Private Triggers
private:
	bool _hasTraveledBack = false;
		
};
