// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Blueprint/UserWidget.h"
#include "DialogueContainer.h"
#include "PlayerUtilities.h"
#include "PlayerJournalManager.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SOUTHGEORGIAPROJECT_API UPlayerJournalManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerJournalManager();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Journal)
		int _gameStage = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Journal)
		bool _hasJournal = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Journal)
		UUserWidget* _journal = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Journal)
		bool _isReadingJournal = false;

	AActor* _journalInteractable;

	bool _hasClosedJournalForFirstTime = false;


	UFUNCTION(BlueprintCallable)
		void ShowJournal();

	UPROPERTY(EditAnywhere)
		UDialogueContainer* _spiritIntroDialogue;


	UFUNCTION(BlueprintCallable)
		void HideJournal();

	UFUNCTION(BlueprintCallable)
		void PickupJournal(UUserWidget* widget, AActor* interactable);

	UPlayerUtilities* _playerutils = nullptr;
};
