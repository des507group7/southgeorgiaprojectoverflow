// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlayerUtilities.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerUtilityHelper.generated.h"

USTRUCT(BlueprintType)
struct FTimePeriodTag
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString _tag;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int _timePeriod;

};

UCLASS()
class SOUTHGEORGIAPROJECT_API APlayerUtilityHelper : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayerUtilityHelper();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void FindAllTimePeriodActors();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerUtils)
		TArray<FTimePeriodActor> _timePeriodActors; //List of all actors that are present in only one time period

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerUtils)
		TArray<FTimePeriodTag> _timePeriodTags; //List of all tags relating to time period actors
};
