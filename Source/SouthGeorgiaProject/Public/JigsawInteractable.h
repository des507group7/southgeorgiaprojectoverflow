// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "JigsawContainer.h"
#include "JigsawInteractable.generated.h"

/**
 * 
 */
UCLASS()
class SOUTHGEORGIAPROJECT_API AJigsawInteractable : public AInteractable
{
	GENERATED_BODY()
public:
	
	AJigsawInteractable();
	virtual void Interact() override;

	UBoxComponent* _myCollider = nullptr;

	UPROPERTY(EditAnywhere)
		UJigsawContainer* JigsawContainer = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UFMODEvent* _beginPuzzleSound;
};
